#include <iostream>
#include <fstream>
#include <string>

#include "version.h"

// program options
#include <boost/program_options.hpp>
namespace po = boost::program_options;

// serialization
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/binary_iarchive.hpp>
//#include <boost/archive/binary_oarchive.hpp>

// openal
#include "presets.h"
#include <efx-creative.h>

#include "presets-file.h"

void do_conversion(const std::string & filename);
void test_deserialization(const std::string & filename);

int main(int argc, char * argv[])
{
	using std::cout;
	using std::endl;

	cout << "EAX to EFX preset conversion tool v" << EAX2EFX_VERSION_STRING << " (" << __DATE__ << ")" << endl << endl;

	// declare supported options
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "show program help")
		("out", "output file name");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help"))
	{
		cout << desc << endl;
		return 1;
	}

	std::string outfilename = "efxpresets.bin";
	if (vm.count("out"))
	{
		outfilename = vm["out"].as<std::string>();
	}
	else
	{
		cout << "Output file name not set, using default." << endl;
	}
	cout << "Output file name: " << outfilename.c_str() << endl;

	do_conversion(outfilename);
	cout << endl;
	test_deserialization(outfilename);

	return 0;
}

void do_conversion(const std::string & filename)
{
	// TODO: make this based on tcc or some other parser in the future?

	// use the Boost serialization mechanism
	PresetsFile pf;
	for (int i = 0; i < eaxPresetsCount; i++)
	{
		pf.names[i] = std::string(eaxPresetsNames[i]);

		// convert
		EAXREVERBPROPERTIES eaxProps = eaxPresets[i];
		EFXEAXREVERBPROPERTIES efxProps;
		std::cout << "Converting " << eaxPresetsNames[i] << " ..." << std::endl;
		ConvertReverbParameters(& eaxProps, & efxProps);
		pf.properties[i] = efxProps;
	}

	// serialize
	{
		// FIX: text archive for x86_64 portability
		std::ofstream ofs(filename.c_str()/*, std::ios::binary*/);
		boost::archive::text_oarchive oa(ofs);
		oa << pf;
	}
	std::cout << "Conversion of " << eaxPresetsCount << " presets successful." << std::endl;
}

void test_deserialization(const std::string & filename)
{
	std::ifstream ifs(filename.c_str()/*, std::ios::binary*/);
	boost::archive::text_iarchive ia(ifs);
	PresetsFile pf;
	ia >> pf;

	std::cout << "Printing deserialized presets..." << std::endl;

	// print the presets
	for (int i = 0; i < eaxPresetsCount; i++)
	{
		std::cout << "Preset " << pf.names[i] << ": " << std::endl;
		std::cout << "{ ";
		std::cout << pf.properties[i].flDensity << ", ";
		std::cout << pf.properties[i].flDiffusion << ", ";
		std::cout << pf.properties[i].flGain << ", ";
		std::cout << pf.properties[i].flGainHF << ", ";
		std::cout << pf.properties[i].flGainLF << ", ";
		std::cout << pf.properties[i].flDecayTime << ", ";
		std::cout << pf.properties[i].flDecayHFRatio << ", ";
		std::cout << pf.properties[i].flDecayLFRatio << ", ";
		std::cout << pf.properties[i].flReflectionsGain << ", ";
		std::cout << pf.properties[i].flReflectionsDelay << ", ";
		// ++
		std::cout << "[";
		std::cout << pf.properties[i].flReflectionsPan[0] << ";";
		std::cout << pf.properties[i].flReflectionsPan[1] << ";";
		std::cout << pf.properties[i].flReflectionsPan[2];
		std::cout << "], ";
		// --
		std::cout << pf.properties[i].flLateReverbGain << ", ";
		std::cout << pf.properties[i].flLateReverbDelay << ", ";
		// ++
		std::cout << "[";
		std::cout << pf.properties[i].flLateReverbPan[0] << ";";
		std::cout << pf.properties[i].flLateReverbPan[1] << ";";
		std::cout << pf.properties[i].flLateReverbPan[2];
		std::cout << "], ";
		// --
		std::cout << pf.properties[i].flEchoTime << ", ";
		std::cout << pf.properties[i].flEchoDepth << ", ";
		std::cout << pf.properties[i].flModulationTime << ", ";
		std::cout << pf.properties[i].flModulationDepth << ", ";
		std::cout << pf.properties[i].flAirAbsorptionGainHF << ", ";
		std::cout << pf.properties[i].flHFReference << ", ";
		std::cout << pf.properties[i].flLFReference << ", ";
		std::cout << pf.properties[i].flRoomRolloffFactor << ", ";
		std::cout << pf.properties[i].iDecayHFLimit;
		std::cout << " }" << std::endl << std::endl;
	}
}
