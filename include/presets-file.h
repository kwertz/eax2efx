#ifndef _presets_file_h_
#define _presets_file_h_

#include <string>

#ifndef EFXEAXREVERBPROPERTIES_DEFINED
#define EFXEAXREVERBPROPERTIES_DEFINED
typedef struct
{
	float flDensity;
	float flDiffusion;
	float flGain;
	float flGainHF;
	float flGainLF;
	float flDecayTime;
	float flDecayHFRatio;
	float flDecayLFRatio;
	float flReflectionsGain;
	float flReflectionsDelay;
	float flReflectionsPan[3];
	float flLateReverbGain;
	float flLateReverbDelay;
	float flLateReverbPan[3];
	float flEchoTime;
	float flEchoDepth;
	float flModulationTime;
	float flModulationDepth;
	float flAirAbsorptionGainHF;
	float flHFReference;
	float flLFReference;
	float flRoomRolloffFactor;
	int	iDecayHFLimit;
} EFXEAXREVERBPROPERTIES, *LPEFXEAXREVERBPROPERTIES;
#endif

// defines the data structure used for serialization/deserialization of the EFX presets
struct PresetsFile
{
	std::string names[eaxPresetsCount];
	EFXEAXREVERBPROPERTIES properties[eaxPresetsCount];
};

namespace boost {
namespace serialization {

	template <class Archive>
	void serialize(Archive & ar, EFXEAXREVERBPROPERTIES & p, const unsigned int version)
	{
		ar & p.flDensity;
		ar & p.flDiffusion;
		ar & p.flGain;
		ar & p.flGainHF;
		ar & p.flGainLF;
		ar & p.flDecayTime;
		ar & p.flDecayHFRatio;
		ar & p.flDecayLFRatio;
		ar & p.flReflectionsGain;
		ar & p.flReflectionsDelay;
		ar & p.flReflectionsPan;
		ar & p.flLateReverbGain;
		ar & p.flLateReverbDelay;
		ar & p.flLateReverbPan;
		ar & p.flEchoTime;
		ar & p.flEchoDepth;
		ar & p.flModulationTime;
		ar & p.flModulationDepth;
		ar & p.flAirAbsorptionGainHF;
		ar & p.flHFReference;
		ar & p.flLFReference;
		ar & p.flRoomRolloffFactor;
		ar & p.iDecayHFLimit;
	}

	template <class Archive>
	void serialize(Archive & ar, PresetsFile & f, const unsigned int version)
	{
		ar & f.names;
		ar & f.properties;
	}

} // namespace serialization
} // namespace boost

#endif // _presets_file_h_